import React, { Component } from "react";
import { Modal, ModalHeader, ModalBody, Button } from "reactstrap";
import DayPicker, { DateUtils } from "react-day-picker";
import "react-day-picker/lib/style.css";
import moment from "moment";

import axios from "axios";
import CheckoutForm from './CheckoutForm';
import { Elements, StripeProvider } from "react-stripe-elements";


class AddForm extends Component {
  state = {
    from: undefined,
    to: undefined,
    totalPayment: 0,
   
  };

  

  handleDayClick = (day, modifiers = {}) => {
    //console.log(range);
    if (modifiers.disabled) {
      this.setState({
        from: undefined,
        to: undefined
      });
      return;
    } else {
      const range = DateUtils.addDayToRange(day, this.state);
      this.setState(range)
    //   console.log(range.to)
        if(range.from == undefined || range.to == undefined){
            const totalPayment = 0;
            this.setState({totalPayment})
        }else{
            const oneDay = 24 * 60 * 60 * 1000;
            const diffDays = Math.round(Math.abs((range.from - range.to) / oneDay));
            const totalPayment = parseInt(diffDays) * parseInt(this.props.vehicle.rentPrice);
            this.setState({totalPayment})
            
        }
    }
  };

  handleAmount = () => {
    console.log(this.state.to)
  }

  handleResetClick() {
    this.setState({
      from: undefined,
      to: undefined
    });
  }

  render() {
    const { from, to } = this.state;
    const modifiers = { start: from, end: to };

    const todisabledDays = this.props.choosenVehicle;
    //console.log(todisabledDays);

  //   const past = {
	// 	before: new Date()
  // }
  
  

    let a = todisabledDays.map(disabledDays => {
      return {
        after: new Date(
          moment(disabledDays.dateNeeded)
            .subtract(1, "days")
            .format("MM/DD/YY")
        ),
        before: new Date(
          moment(disabledDays.untilDate)
            .add(1, "days")
            .format("MM/DD/YY")
        )
      };
    });

    const today = [new Date()];
      let b = today.map(dd=>{
    return {before: new Date(moment(dd).format("MM/DD/YY"))}
  })
  a.unshift(b[0])

    //console.log(a);
   
    return (
      <div>
        <Modal isOpen={this.props.showForm} toggle={this.props.handleShowForm}>
            <ModalHeader toggle={this.props.handleShowForm}>
                Add Booking
            </ModalHeader>
            <ModalBody>
                <h4>Vehicle: {this.props.vehicle.name}</h4>
                <p>Plate No: {this.props.vehicle.plateNumber}</p>
                <p>Description: {this.props.vehicle.description}</p>
                <p>Rent Price: {this.props.vehicle.rentPrice}</p>
                <p>Choose the date:</p>
                <DayPicker
                    className="Selectable"
                    numberOfMonths={2}
                    onDayClick={this.handleDayClick}
                    selectedDays={[from, { from, to }]}
                    modifiers={modifiers}
                    disabledDays={a}
                />
                <StripeProvider 
                    apiKey="pk_test_0ZLm93tap9XV5dUemOKaCALM00WWezGQ50"
                >
                <div>
                <div className="d-flex justify-content-center">
                  <Elements>
                  <CheckoutForm
                      totalPayment={this.state.totalPayment}
                      from = {this.state.from}
                      to={this.state.to}
                      vehicle={this.props.vehicle}
                      userId={this.props.user.id}
                      userName={this.props.user.name}
                      handleRefresh={this.props.handleRefresh}
                      noUser={this.props.noUser}
                      booked={this.props.booked}
                  />
                  </Elements>
                </div>
              </div>
            </StripeProvider>
            </ModalBody>
        </Modal>
      </div>
    );
  }
}

export default AddForm;