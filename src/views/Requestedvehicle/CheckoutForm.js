import React, {useState, useEffect} from 'react';
import {CardElement, injectStripe} from 'react-stripe-elements';
import moment from 'moment';
import axios from 'axios'
import Swal from 'sweetalert2'

const success = () => {
  Swal.fire({
    position: 'center',
    icon: 'success',
    title: 'Booking Success!',
    showConfirmButton: false,
    timer: 500
  })
}


const CheckoutForm = (props) => {

  const [stripeRequired, setStripeRequired] = useState(false)

  const [user, setUser] = useState("")
  useEffect(() => {
    if(sessionStorage.token){
      let user = JSON.parse(sessionStorage.user)
      setUser(user);
    }
  }, [])

  
  
  
	const submit = async (e) => {
        // console.log(e)
    if(user){    
        
        let {token} = await props.stripe.createToken({
            name:"Name"})
        let tokenId = token.id
        let amount = 10000

        let data = new FormData;
        data.append('amount_decimal', 12345)

		    axios.post('https://agile-cliffs-07835.herokuapp.com/charge',{
            amount: props.totalPayment * 100,
            description: props.totalPayment,
            source: token.id
        }).then(res=>{
            console.log(res)
            const oneDay = 24 * 60 * 60 * 1000;
            const firstDate = new Date(props.from);
            const secondDate = new Date(props.to);
            const diffDays = Math.round(Math.abs((firstDate - secondDate) / oneDay));
            
            const totalPayment = parseInt(diffDays) * parseInt(props.vehicle.rentPrice);
            
            const user = props.userName;
            const id = props.userId;
            let code = moment(new Date()).format("x");
            // props.booked();
            
            axios({
                method: "POST",
                url: "https://agile-cliffs-07835.herokuapp.com/admin/addVehicleRequest",
                data: {
                  requestor: user,
                  requestorId: id,
                  requestedVehicles: props.vehicle.name,
                  requestedVehicleId: props.vehicle._id,
                  requestedPlateno: props.vehicle.plateNumber,
                  image:props.vehicle.image,
                  dateNeeded: moment(props.from).format("MM/DD/YYYY"),
                  untilDate: moment(props.to).format("MM/DD/YYYY"),
                  code: code,
                  totalPrice: totalPayment
                },
                
              })
              Swal.fire({
                icon: 'success',
                title: 'You successfully booked',
                showConfirmButton: false,
                timer: 1500
              }) 
              .then(res => {
                console.log(res.data);
                props.handleRefresh();
                window.location.replace('#/userrequest')
              });
            })
    }else{
      props.noUser();
      props.handleRefresh();
      
    }        
  }
  
  const handleonChange = (e) => {
		if(e.complete === false){
			setStripeRequired(false)
		}else {
			setStripeRequired(true)
		}

	}


	return (
		<div className="checkout">
			<p>Amount: {props.totalPayment}</p>
        	<p>Would you like to complete the purchase?</p>
        	<CardElement onChange={handleonChange}/>
        	<button
          disabled={props.totalPayment === 0 || stripeRequired === false ? true : false} 
          onClick={submit}>Book</button>
      	</div>
  )

}

export default injectStripe(CheckoutForm);



