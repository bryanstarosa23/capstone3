import React, {useEffect, useState} from 'react';
import axios from 'axios'
import {Navbar, Footer} from '../Layout'
import Vehicles from './Vehicles'
import RequestRow from './RequestRow'
import moment from 'moment'
import AddForm from './AddForm'
import {ToastContainer, toast} from 'react-toastify';


const Requests =()=>{
    const [vehicles, setVehicles] = useState([]);
    const [showForm, setShowForm] = useState(false);
    const [user, setUser] = useState({});
    const [showStatus, setShowStatus] = useState(false);
    const [editId, setEditId] = useState("");
	const [showQuantity, setShowQuantity] = useState(false);
	const [choosenVehicle, setChoosenVehicle] = useState([]);
	const [vehicle, setVehicle] = useState([]);
	
	const noUser = () =>{
		toast.error("Please Login")
	  }
	const booked = () =>{
		toast.success("Successfully Booked")
	}
	
    const handleRefresh = () => {
		setShowForm(false);
		setEditId("");
	}

    useEffect(()=>{
		axios.get('https://agile-cliffs-07835.herokuapp.com/admin/showvehicles'
			).then(res=>{
            setVehicles(res.data);
		})
    },[])
	
	const handleShowForm = (vehicleId) =>{
		setShowForm(!showForm)
		axios.get("https://agile-cliffs-07835.herokuapp.com/admin/showvehiclebyid/" +vehicleId
		).then(res=>{
			console.log(res.data)
			setVehicle(res.data)
		})
		axios.get("https://agile-cliffs-07835.herokuapp.com/admin/showvehiclerequestsbyvehicleid/" +vehicleId
		).then(res=>{
			console.log(res.data)
			setChoosenVehicle(res.data)
        })
    }

    useEffect(()=>{
		if(sessionStorage.token){
			let user = JSON.parse(sessionStorage.user)
			setUser(user);
		}else{
			let user = {
				name:"guest",
				id:"guest454541125"
			}
			setUser(user)
		}
    }, [])
   
    return(
        <>
        <Navbar/>
		<ToastContainer />
            <div className="text-center mb-5">
				<h1 className="text-center py-5">Choose your Car</h1>
				<div className="container d-flex flex-wrap justify-content-around">
                    {vehicles.map(vehicle=>(
                        <Vehicles
                            key={vehicle._id}
                            vehicle={vehicle}
							handleShowForm={handleShowForm}
                        />
                    ))}
                </div>
                <AddForm
					showForm={showForm}
					handleRefresh={handleRefresh}
					handleShowForm={handleShowForm}
					choosenVehicle={choosenVehicle}
					vehicles={vehicles}
					vehicle={vehicle}
					user={user}
					handleRefresh={handleRefresh}
					noUser={noUser}
					booked={booked}
			
				/>
			</div>       
        <Footer/> 
        </>       
    )
}

export default Requests;