import React, {useState, useEffect} from 'react';
import {Card, CardImg, CardText, CardBody,CardTitle, Button,CardSubtitle} from 'reactstrap';
import AddForm from './AddForm';
import axios from 'axios';

  
  
const Vehicles = (props) => { 
    

    const vehicle = props.vehicle;
    
    

    return (
      <>
        <Card key={props.vehicle._id} className="cardSize mb-4">
            <CardImg top width="100%" src={"https://agile-cliffs-07835.herokuapp.com/"+vehicle.image} alt="Card image cap" className="imahe" />
            <CardBody>
            <CardTitle><h5><strong>{vehicle.name}</strong></h5></CardTitle>
            <CardSubtitle className="pb-1"><strong>Features:</strong> {vehicle.description}</CardSubtitle>
            <CardSubtitle className="pb-1"><strong>Rent Price:</strong> {vehicle.rentPrice}</CardSubtitle>
            <CardSubtitle className="pb-1"><strong>Category:</strong> {vehicle.category}</CardSubtitle>
            <CardSubtitle className="pb-1"><strong>Code:</strong> {vehicle.code}</CardSubtitle>
            <Button className="bg-success"
            onClick={()=>props.handleShowForm(vehicle._id)}
            >Book</Button>
            </CardBody>
        </Card> 
      </>
    );
  };
  
  export default Vehicles;