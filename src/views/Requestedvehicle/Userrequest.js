import React, {useEffect, useState} from 'react';
import axios from 'axios'
import {Navbar, Footer} from '../Layout'
import RequestRow from './RequestRow'
import moment from 'moment'
import Swal from 'sweetalert2'
import CsvDownload from 'react-json-to-csv';


const Requests =()=>{
    // const [vehicles, setVehicles] = useState([]);
    const [requests, setRequests] = useState([])
    const [showForm, setShowForm] = useState(false);
    const [user, setUser] = useState({});
    const [showStatus, setShowStatus] = useState(false);
    const [editId, setEditId] = useState("");
    const [showQuantity, setShowQuantity] = useState(false);

    const handleRefresh = () => {
		setShowForm(false);
		setEditId("");
	}

    // useEffect(()=>{
	// 	axios.get('http://localhost:4000/admin/showvehicles'
	// 		).then(res=>{
    //         setVehicles(res.data);
	// 	})
	// },[])
	
    useEffect(()=>{
		if(sessionStorage.token){
			let user = JSON.parse(sessionStorage.user)
			if(user.isAdmin){
				axios.get('https://agile-cliffs-07835.herokuapp.com/admin/showvehiclerequests').then(res=>{
				setRequests(res.data)
				})
			}else{
				axios.get('https://agile-cliffs-07835.herokuapp.com/admin/showvehiclerequestsbyuser/'+user.id).then(res=>{
					setRequests(res.data)
					console.log(res.data)
				})
			}
			setUser(user);
		}else{
			window.location.replace('#/login')
		}
    }, []);
	
	const swalWithBootstrapButtons = Swal.mixin({
		customClass: {
		  confirmButton: 'btn btn-success',
		  cancelButton: 'btn btn-danger'
		},
		buttonsStyling: false
	  })
	
	  const handleDeleteRequest = requestId => {
		swalWithBootstrapButtons.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonText: 'Yes, delete it!',
		  cancelButtonText: 'No, cancel!',
		  reverseButtons: true
		}).then((result) => {
		  if (result.value) {
			swalWithBootstrapButtons.fire(
			  'Deleted!',
			  'Your file has been deleted.',
			  'success'
			)
			axios.delete('https://agile-cliffs-07835.herokuapp.com/admin/deletevehiclerequest/'+requestId).then(res=>{
			let index = requests.findIndex(request=>request._id===requestId);
			let newRequests = [...requests];
			newRequests.splice(index,1);
			setRequests(newRequests);
			})
		  } else if (
			/* Read more about handling dismissals below */
			result.dismiss === Swal.DismissReason.cancel
		  ) {
			swalWithBootstrapButtons.fire(
			  'Cancelled',
			  'Your file is safe :)',
			  'error'
			)
		  }
		})
	  }

    // const handleDeleteRequest = (requestId) => {
	// 	axios.delete('http://localhost:4000/admin/deletevehiclerequest/'+requestId).then(res=>{
	// 		let index = requests.findIndex(request=>request._id===requestId);
	// 		let newRequests = [...requests];
	// 		newRequests.splice(index,1);
	// 		setRequests(newRequests);
	// 	})
    // }
    const handleShowStatus = (editId) => {
		setEditId(editId);
		setShowStatus(true);
		setShowQuantity(false);
		// console.log(editId)
    }
    const handleStatusInput = (status, logs) =>{

		let log = {
			status:"pending",
			updateDate: moment(new Date).format("MM/DD/YYYY"),
			message: "Updated Status to " + status	
		}

		axios.patch('https://agile-cliffs-07835.herokuapp.com/admin/updatevehiclerequeststatus/'+editId,{
			status,
			logs: log
		}).then(res=>{
			let index = requests.findIndex(request=>request._id==editId);
			console.log(res.data)
			let newRequests = [...requests];
			newRequests.splice(index,1,res.data);
			setRequests(newRequests);
			handleRefresh();
		})
    }
    const handleShowQuantity = (editId) =>{
		setEditId(editId);
		setShowQuantity(true);
		setShowStatus(false);
	}
    return(
        <>
        <Navbar/>
            <div className="text-center">
				{/* <h1 className="text-center py-5">Choose your Car</h1>
				<div className="container d-flex">
                    {vehicles.map(vehicle=>(
                        <Vehicles
                            key={vehicle._id}
                            vehicle={vehicle}
                        />
                    ))}
                </div> */}
                <h2 className="text-center py-5">Booking Details</h2>
				{user.isAdmin ?
                <CsvDownload data={requests} />
            	:
            	""
            	}
				<div className="col-lg-10 offset-lg-1">
					<table className="table table-striped border">
						<thead>
							<tr className="bg-dark text-white">
								<th>Request Code</th>
								<th>Date Requested</th>
								<th>Requested Vehicle</th>
								<th>Plate Number</th>
								<th>Requested By</th>
								<th>Date Needed</th>
								<th>Until</th>
								<th>Total Price</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							{requests.map(request=>
							<RequestRow 
								key={request._id}
								request={request}
								handleDeleteRequest={handleDeleteRequest}
								handleShowStatus={handleShowStatus}
								showStatus={showStatus}
								editId={editId}
								handleStatusInput={handleStatusInput}
								handleShowQuantity={handleShowQuantity}
								showQuantity={showQuantity}
								user={user}
							/>
                            )
							}
						</tbody>
					</table>
				</div>
			</div>        
        <Footer/> 
        </>       
    )
}

export default Requests;
