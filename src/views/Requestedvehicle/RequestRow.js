import React, {useState} from 'react';
import {
	Button,
} from 'reactstrap';
import Logs from './Logs';


const RequestRow = (props) => {

	const [showLogs, setShowLogs] = useState(false);
	const [dropdownOpen, setDropdownOpen] = useState(false)

	const toggle = () => setDropdownOpen(!dropdownOpen)

	const request = props.request;

	const handleShowLogs = () => {
		setShowLogs(!showLogs);
	}

	return (
		<React.Fragment>
		<tr>
			<td>{request.code}</td>
			<td>{request.dateRequested}</td>
			<td>{request.requestedVehicles}</td>
			<td>{request.requestedPlateno}</td>
			<td>{request.requestor}</td>
			<td>{request.dateNeeded}</td>
			<td>{request.untilDate}</td>
			<td>{request.totalPrice}</td>
			{/* <td
				onClick={()=>props.handleShowStatus(request._id)}
			>{props.user.isAdmin && props.showStatus && props.editId == request._id
				? 
				<Dropdown
					isOpen={dropdownOpen}
					toggle={toggle}
				>
					<DropdownToggle caret>
					Update Status
					</DropdownToggle>
					<DropdownMenu>
						<DropdownItem 
						onClick={()=>props.handleStatusInput("Processing", request.logs)}>Processing</DropdownItem>
						<DropdownItem
						onClick={()=>props.handleStatusInput("Approved", request.logs)}
						>Approved</DropdownItem>
						<DropdownItem
						onClick={()=>props.handleStatusInput("Rejected", request.logs)}
						>Rejected</DropdownItem>
					</DropdownMenu>
				</Dropdown>
				: request.status
			}</td> */}
			<td>
				<Button
					color="danger"
					onClick={()=>props.handleDeleteRequest(request._id)}
				>Cancel</Button>{" "}
				{/* <Button
					color="info"
					onClick={handleShowLogs}
				>View Logs</Button> */}
			</td>
		</tr>
		<Logs 
			handleShowLogs={handleShowLogs}
			showLogs={showLogs}
			request={request}
		/>
		</React.Fragment>
		)
}

export default RequestRow;