import React, {useState} from 'react';
import {
	Button
} from 'reactstrap';
import axios from 'axios'
import toastify from 'react-toastify'
import moment from 'moment'

const Images = props => {
	const [imagesToSave, setImages] = useState([]);
	const [infoMessage, setMessage] = useState("");
	const [imageUrls, setImageUrls] = useState([])
	const [vehicleImage, setVehicleImage] = useState(false)

	
	const vehicles = props.vehicles;
	const selectImages = e => {
		let images = [];
		for(let i=0; i<e.target.files.length; i++){
			images[i]= e.target.files.item(i)
		}

		images = images.filter(image=>image.name.match(/\.(jpg|JPG|JPEG|jpeg|png|gif)$/))
		let message= `${images.length} valid image(s) selected`;

		setImages(images);
		setMessage(message);
		setVehicleImage(!vehicleImage)
	}

	const uploadImages = () => {
		let urls = [...imageUrls]
		const uploaders = imagesToSave.map(image=>{
			const data = new FormData();
			data.append("image", image, image.name);			
			

			return axios.post('https://agile-cliffs-07835.herokuapp.com/upload', data).then(res=>{
				urls.push(res.data.imageUrl)
				const vehicleimage = (res.data.imageUrl)
				
				setImageUrls(urls)
				let sameplate = false;
				vehicles.map(vehicle=>{
					if(vehicle.plateNumber == props.plateno){
						sameplate = true;
					}
				})
				if(sameplate==true){
					console.log("sorry plate " + props.plateno + " already exists")
					props.invalidPlateNo();
				}else{
					let code = moment(new Date()).format("x");
					axios({
						method: "POST",
						url: "https://agile-cliffs-07835.herokuapp.com/admin/addvehicle",
						data:{
							name:props.name,
							description:props.description,
							category:props.categoryName,
							plateNumber:props.plateno,
							rentPrice:props.rentPrice,
							image:vehicleimage,
							code:code	
						}
					}).then(res=>{
						
						let newVehicles = [...vehicles];
						newVehicles.push(res.data);
						props.setVehicles(newVehicles);
			
					});
			
					props.handleRefresh();
				}
			})
		})
	}
	
	
	console.log(imageUrls)
	return (
		<React.Fragment>
			<div className="pb-3 d-flex flex-column">
				<input 
					className="form-control" 
					type="file" 
					multiple 
					onChange={selectImages}
				/>
				<small>{infoMessage}</small>
				<Button
					className="my-3"
					color="primary"
					onClick={uploadImages}
					disabled={props.categoryName!="" && props.name!="" && props.description != "" && props.plateno != "" && vehicleImage != "" && props.rentPrice != 0 ? false : true}
				>
					Save
				</Button>
				{/* <div>
					{
						imageUrls.map((url, index)=>(
							<div key={index}>
								<img 
									src={"http://localhost:4000/"+url}
									alt="Not available"
								/>
							</div>
						))
					}
				</div> */}
			</div>
		</React.Fragment>
	)
}

export default Images;