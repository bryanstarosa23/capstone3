import React, {useEffect, useState} from 'react';
import {
  Link
} from 'react-router-dom';
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';

const Navbar = ({className,...props}) => {
	const [user, setUser] = useState("")
 	useEffect(() => {
    if(sessionStorage.token){
      let user = JSON.parse(sessionStorage.user)
      setUser(user);
    }
  }, [])

  	const handleLogout = () => {
    sessionStorage.clear()
    window.location.replace('/')
  }
  
	return (
		<React.Fragment>
			<nav class="navbar navbar-expand-lg navbar-dark bg-dark color-white g">
			  <a class="navbar-brand" href="#">
			  <span>RENTOGO</span></a>
			  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
			    <span class="navbar-toggler-icon"></span>
			  </button>
			
			  <div class="collapse navbar-collapse" id="navbarColor01">
               {user ?
			    <ul class="navbar-nav mr-auto">
					<li class="nav-item active d-flex justify-content-center align-items-center px-2 mr-auto">
								<Link to="/"
								className="text-white"
								>Home</Link>
							</li>
				</ul>
               :
			   <ul class="navbar-nav mr-auto">
			      <li class="nav-item d-flex justify-content-center align-items-center px-2 ml-auto">
			        <Link to="/requestedvehicles"
			        	className="text-white"
			        >Cars to Rent</Link>
			      </li>
			   	</ul>
               }
					{user
					?
               <>
			   <ul class="navbar-nav ml-auto">
					<li class="nav-item d-flex justify-content-center align-items-center px-2">
						<Link to="/requestedvehicles"
							className="text-white"
						>Cars to Rent</Link>
			      	</li>
               </ul>
			   <ul class="navbar-nav">
					<li className="nav-item">
						<UncontrolledDropdown nav inNavbar>
							<DropdownToggle nav caret>
								Hi! {user.name}
							</DropdownToggle>
							{sessionStorage.token != undefined && user.isAdmin == true ? (
								<DropdownMenu right>
								<DropdownItem href="#/vehicles">
									Vehicles
								</DropdownItem>
								<DropdownItem href="#/userrequest">
									All Bookings
									</DropdownItem>
								<DropdownItem divider />
								<DropdownItem onClick={handleLogout}>Logout</DropdownItem>
								</DropdownMenu>
					
							) : (
								""
							)}

							{sessionStorage.token != undefined && user.isAdmin == false ? (
								<DropdownMenu right>
								<DropdownItem href="#/userrequest">
									All Bookings
								</DropdownItem>
								<DropdownItem onClick={handleLogout}>Logout</DropdownItem>
								</DropdownMenu>
							) : (
								""
							)}
					   </UncontrolledDropdown>
					</li>
				</ul>
               </>
					:
				<ul class="navbar-nav ml-auto">
                  <React.Fragment>
                  <li className="nav-item">
                     <Link className="nav-link" to="/register">Register</Link>
                  </li>
                  <li className="nav-item">
                     <Link className="nav-link" to="/login">Login</Link>
                  </li>
                  </React.Fragment>
			    </ul>
				   }
               
			
			  </div>
			
			</nav>
		</React.Fragment>
	)
}

export default Navbar;