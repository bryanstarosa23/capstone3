import React from 'react';


const Footer = () => {
	return (
		<>
			<footer class="secFooter">
			<div class="secFooterContent">
				<div class="secFooterIcons">
					<a href="https://www.facebook.com/" target="blank">
						<div class="footerIcon">
							<i class="fab fa-facebook"></i>
							<span>Facebook</span>
						</div>
					</a>
					<a href="https://www.Instagram.com/" target="blank">
						<div class="footerIcon">
							<i class="fab fa-instagram"></i>
							<span>Instagram</span>
						</div>
					</a>
					<a href="https://plus.google.com/" target="blank">
						<div class="footerIcon">
							<i class="fab fa-google-plus"></i>
							<span>Google+</span>
						</div>
					</a>
				</div>
				
				
				<div>
				<hr class="hrstyle"/></div>
        <div class="d-flex">
            <div class="footerCredits">
              <i class="far fa-copyright"></i>
              Muntinlupa City
            </div>
    				<div class="footerCredits mx-5">
    					<i class="far fa-copyright"></i>
    					RENTOGO
    				</div>
            <div class="footerCredits">
              <i class="far fa-copyright"></i>
              062-005-0818
            </div>
        </div>
			</div>			
		</footer>

		</>
	)
}

export default Footer