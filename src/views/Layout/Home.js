import React, {useEffect, useState} from 'react';
import {
	Navbar,
	Footer
} from '../Layout';
import {Button} from 'react-bootstrap';
import {
	Link
  } from 'react-router-dom';



const Home = () => {
	// const [user, setUser] = useState({})

	// useEffect(()=>{
	// 	if(sessionStorage.token){
	// 		let user = JSON.parse(sessionStorage.user)
	// 		console.log(user);
	// 		setUser(user);
	// 	}else{
	// 		window.location.replace("#/requestedvehicles");
	// 	}
	// }, [])

	return (
		<React.Fragment>
			<Navbar />
				<div className="d-flex flex-column justify-content-center align-items-center bgimage colorwhite">
				{/* <img src={require("../Images/LandingPage.jpeg")} alt="X" 
					style={styles.backgroundImage} 
				/> */}
				<h1 className="bigSize py-3 px-3">RENTOGO</h1>
				<br/>
				<Button>
				<li class="nav-item active d-flex justify-content-center align-items-center mt-1">
					<Link to="/requestedvehicles"
			        	className="text-white"
			        ><h6>Book a Car Now</h6></Link>	</li>
				</Button>
				
				</div>
			<Footer />
		</React.Fragment>
	)
}

export default Home;