import React, {useEffect, useState} from 'react';
import {VehicleRow, VehicleForm, VehicleStock} from './components';
import {Loading} from '../../globalcomponents';
import {
	Button
} from 'reactstrap';
import axios from 'axios';
import {ToastContainer, toast} from 'react-toastify';
import {
	Navbar, Footer
} from "../Layout"
import Swal from 'sweetalert2'


const invalidPlateNo = () =>{
	toast.error("Plate No. already exist")
}

const invalidDescription = () =>{
	toast.error("Please put some description")
}

const invalidName = () => {
	toast.error("Please input the name")
}
const invalidRentPrice = () => {
	toast.error("Please input a price")
}

const Vehicles = () => {
	const [vehicles, setVehicles] = useState([]);
	const [isLoading, setIsLoading] = useState(true);
	const [showForm, setShowForm]= useState(false);

	const [nameRequired, setNameRequired] = useState(true);
	const [name, setName] = useState("");
	const [showName, setShowName] = useState(true);

	const [descriptionRequired, setDescriptionRequired] = useState(true);
	const [description, setDescription] = useState("");
	const [showDescription, setShowDescription] = useState(true);

	const [showRentPrice, setShowRentPrice] = useState(true);
	const [rentPriceRequired, setRentPriceRequired] = useState(true);
	const [rentPrice, setRentPrice] = useState(0);

	const [categoryRequired, setCategoryRequired]=useState(true);
	const [category, setCategory] = useState("");
	const [showCategory, setShowCategory] = useState(true);
	
	const [editId, setEditId] = useState("");
	const [user, setUser] = useState({})
	const [plateno, setPlateno] = useState("");
	const [platenoRequired, setPlateNoRequired] = useState(true);
	const [categoryName, setCategoryName] = useState("");

	


	const handleRefresh = () => {
		setShowForm(false);
		setName("");
		setCategoryName("");
		setDescription("");
		setNameRequired(true);
		setCategoryRequired(true);
		setDescriptionRequired(true);
		setPlateNoRequired(true);
		setPlateno("");
	}

	useEffect(()=>{
		// fetch('')

		if(sessionStorage.token){
			let user = JSON.parse(sessionStorage.user);
			if(user.isAdmin){
				fetch('https://agile-cliffs-07835.herokuapp.com/admin/showvehicles'
				).then(response=>response.json()
				).then(data=>{
					setVehicles(data);
					setIsLoading(false);
				});

				setUser(user);

			}else{
				window.location.replace('/')
			}
		}else{
			window.location.replace('#/login')
		}
	},[])

	const handleShowForm = () =>{
		setShowForm(!showForm)
		setPlateNoRequired(true)
		setNameRequired(true)
		setCategoryRequired(true)
		setDescriptionRequired(true)
		setRentPriceRequired(true)
	}

	const handleVehicleNameChange = e => {
		if(e.target.value==""){
			setNameRequired(true);
			setName("")
		}else{
			setNameRequired(false)
			setName(e.target.value);
		}
	}

	const handleRentPriceChange = e => {
		if(e.target.value==0){
			setRentPriceRequired(true);
			setRentPrice(0)
		}else{
			setRentPriceRequired(false)
			setRentPrice(e.target.value);
		}
	}

	const handleVehicleCategoryChange = e => {
		if(e.target.value==""){
			setCategoryRequired(true);
			setCategoryName("")
		}else{
			setCategoryRequired(false)
			setCategoryName(e.target.value);
		}
	}

	const handleVehicleDescriptionChange = e => {
		if(e.target.value==""){
			setDescriptionRequired(true);
			setDescription("")
		}else{
			setDescriptionRequired(false)
			setDescription(e.target.value);
		}
	}

	const handlePlateNoChange = e => {
		if(e.target.value==""){
			setPlateNoRequired(true);
			setPlateno("")
		}else{
			setPlateNoRequired(false)
			setPlateno(e.target.value);
		}
	}


	const handleSaveVehicle = () => {
		let sameplate = false;
		vehicles.map(vehicle=>{
			if(vehicle.plateNumber == plateno){
				sameplate = true;
			}
		})
		if(sameplate==true){
			console.log("sorry plate " + plateno + " already exists")
		}else{
			axios({
				method: "POST",
				url: "https://agile-cliffs-07835.herokuapp.com/admin/addvehicle",
				data:{
					name:name,
					description:description,
					category:categoryName,
					plateNumber:plateno,
				}
			}).then(res=>{
				
				let newVehicles = [...vehicles];
				newVehicles.push(res.data);
				setVehicles(newVehicles);
	
			});
	
			handleRefresh();
		}
		
		
	}

	// const handleDeleteVehicle = vehicleId => {
	// 	axios({
	// 		method: "DELETE",
	// 		url: "http://localhost:4000/admin/deletevehicle/" + vehicleId
	// 	}).then(res=>{
	// 		let newVehicles = vehicles.filter(vehicle=>vehicle._id!=vehicleId);
	// 		setVehicles(newVehicles)
	// 	});
	// }

	const swalWithBootstrapButtons = Swal.mixin({
		customClass: {
		  confirmButton: 'btn btn-success',
		  cancelButton: 'btn btn-danger'
		},
		buttonsStyling: false
	  })
	
	  const handleDeleteVehicle = vehicleId => {
		swalWithBootstrapButtons.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonText: 'Yes, delete it!',
		  cancelButtonText: 'No, cancel!',
		  reverseButtons: true
		}).then((result) => {
		  if (result.value) {
			swalWithBootstrapButtons.fire(
			  'Deleted!',
			  'Your file has been deleted.',
			  'success'
			)
			axios({
				method: "DELETE",
				url: "https://agile-cliffs-07835.herokuapp.com/admin/deletevehicle/" + vehicleId
			}).then(res=>{
				let newVehicles = vehicles.filter(vehicle=>vehicle._id!=vehicleId);
				setVehicles(newVehicles)
			});
		  } else if (
			/* Read more about handling dismissals below */
			result.dismiss === Swal.DismissReason.cancel
		  ) {
			swalWithBootstrapButtons.fire(
			  'Cancelled',
			  'Your file is safe :)',
			  'error'
			)
		  }
		})
	  }

	const handleEditName = (e, vehicleId) => {
		let name = e.target.value;
		// console.log(description)
		if(name == ""){
			setShowName(true);
			invalidName();
		}else{
			axios({
				method:'PATCH',
				url: 'https://agile-cliffs-07835.herokuapp.com/admin/updatename/'+vehicleId,
				data: {
					name: name
				}
			}).then(res=>{
				let oldIndex;
				vehicles.forEach((vehicle,index)=>{
					if(vehicle._id === vehicleId){
						oldIndex=index;
					}
				});

				let newVehicles = [...vehicles];
				newVehicles.splice(oldIndex, 1, res.data);
				setVehicles(newVehicles);
				setShowDescription(true);
				setEditId("");
			})
		}
	}

	const handleEditDescription = (e, vehicleId) => {
		let description = e.target.value;
		// console.log(description)
		if(description == ""){
			setShowDescription(true);
			invalidDescription();
		}else{
			axios({
				method:'PATCH',
				url: 'https://agile-cliffs-07835.herokuapp.com/admin/updatevehicledescription/'+vehicleId,
				data: {
					description: description
				}
			}).then(res=>{
				let oldIndex;
				vehicles.forEach((vehicle,index)=>{
					if(vehicle._id === vehicleId){
						oldIndex=index;
					}
				});

				let newVehicles = [...vehicles];
				newVehicles.splice(oldIndex, 1, res.data);
				setVehicles(newVehicles);
				setShowDescription(true);
				setEditId("");
			})
		}
	}

	const handleEditRentPrice = (e, vehicleId) => {
		let rentPrice = e.target.value;
		// console.log(description)
		if(rentPrice == 0){
			setShowRentPrice(true);
			invalidRentPrice();
		}else{
			axios({
				method:'PATCH',
				url: 'https://agile-cliffs-07835.herokuapp.com/admin/updatevehiclerentprice/'+vehicleId,
				data: {
					rentPrice: rentPrice
				}
			}).then(res=>{
				let oldIndex;
				vehicles.forEach((vehicle,index)=>{
					if(vehicle._id === vehicleId){
						oldIndex=index;
					}
				});

				let newVehicles = [...vehicles];
				newVehicles.splice(oldIndex, 1, res.data);
				setVehicles(newVehicles);
				setShowDescription(true);
				setEditId("");
			})
		}
	}

	const handleEditCategory = category => {
		axios({
			method: "PATCH",
			url: "https://agile-cliffs-07835.herokuapp.com/admin/updatevehiclecategory/" + editId,
			data: {
				category: category
			}
		}).then(res=>{
			let oldIndex;
				vehicles.forEach((vehicle,index)=>{
					if(vehicle._id === editId){
						oldIndex=index;
					}
				});

				let newVehicles = [...vehicles];
				newVehicles.splice(oldIndex, 1, res.data);
				setVehicles(newVehicles);
				setShowDescription(true);
				setEditId("");
		})
	}

	const handleDescriptionEditInput = editId => {
		setEditId(editId)
		setShowDescription(true)
		setShowCategory(false)
		setShowName(false);
		setShowRentPrice(false);
	}

	const handleRentPriceEditInput = editId => {
		setEditId(editId)
		setShowDescription(false)
		setShowCategory(false)
		setShowName(false);
		setShowRentPrice(true);
	}

	const handleCategoryEditInput = editId => {
		setEditId(editId);
		setShowDescription(false);
		setShowCategory(true);
		setShowName(false);
		setShowRentPrice(false);
	}

	const handleNameEditInput = editId => {
		setEditId(editId);
		setShowDescription(false);
		setShowCategory(false);
		setShowName(true);
		setShowRentPrice(false);
	}

	const handleCategoryInput = (e) => {
		// console.log(e)
		setCategoryName(e);
	}


	return (
		<React.Fragment>
		<ToastContainer />
		{isLoading ?
			<Loading />
		:
		<React.Fragment>
		<Navbar />
			
			<div>
				<div className="d-flex flex-column justify-content-center pr-5 pb-4">
					<h1 className="text-center py-5">Vehicles</h1>
					<table className="table table-striped border col-lg-4 offset-lg-4">
					<thead>
						<tr className="text-center">
							<th>Category</th>
							<th>Stock</th>
						</tr>
					</thead>
					<tbody>
						<VehicleStock 
							vehicles={vehicles}
						/>
					</tbody>
				</table>
				</div>
				<div className="d-flex justify-content-center pr-5 pb-4">
					<Button
					color="success"
					onClick={handleShowForm}
					>+ Add Vehicle</Button>
					<VehicleForm 
						showForm={showForm}
						handleShowForm={handleShowForm}

						handleVehicleNameChange={handleVehicleNameChange}
						nameRequired={nameRequired}

						handleVehicleDescriptionChange={handleVehicleDescriptionChange}
						descriptionRequired={descriptionRequired}

						handleVehicleCategoryChange={handleVehicleCategoryChange}
						categoryRequired={categoryRequired}
						categoryName={categoryName}
						category={category}
						name={name}
						description={description}
						plateno={plateno}
						platenoRequired={platenoRequired}
						handleSaveVehicle={handleSaveVehicle}

						handleCategoryInput={handleCategoryInput}
						handlePlateNoChange={handlePlateNoChange}

						vehicles={vehicles}
						handleRefresh={handleRefresh}

						handleRentPriceChange={handleRentPriceChange}
						rentPriceRequired={rentPriceRequired}
						rentPrice={rentPrice}
						invalidPlateNo={invalidPlateNo}

						setVehicles = {setVehicles}
					/>
				</div>
			</div>
			<table
				className="table table-striped border col-lg-10 offset-lg-1"
			>
				<thead>
					<tr className="text-center">
						<th>Plate No.</th>
						<th>Name</th>
						<th>Description</th>
						<th>Category</th>
						<th>Rent Price</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{vehicles.map(vehicle=>
						<VehicleRow 
							//showVehicles
							key={vehicle._id}
							vehicle={vehicle}
							//deleteVehicles
							handleDeleteVehicle={handleDeleteVehicle}

							editId={editId}
							handleDescriptionEditInput={handleDescriptionEditInput}
							showDescription={showDescription}
							handleEditDescription={handleEditDescription}
							
							handleCategoryEditInput={handleCategoryEditInput}
							showCategory={showCategory}
							handleEditCategory={handleEditCategory}

							handleRentPriceEditInput={handleRentPriceEditInput}
							showRentPrice={showRentPrice}
							handleEditRentPrice={handleEditRentPrice}
							
							handleNameEditInput={handleNameEditInput}
							showName={showName}
							handleEditName={handleEditName}
						/>
					)} 
				</tbody>
			</table>
			<Footer />
		</React.Fragment>
		}
		</React.Fragment>
	)
}

export default Vehicles;