import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {
	FormGroup,
	Label,
	Dropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem
} from 'reactstrap'

const CategoryInput = (props) => {

	const [dropdownOpen, setDropdownOpen] = useState(false)
	const toggle = () => setDropdownOpen(!dropdownOpen)

	return (
		<FormGroup>
			<Label>Category Name:</Label>
			<Dropdown
				isOpen={dropdownOpen}
				toggle={toggle}
			>
				<DropdownToggle caret>
					{props.categoryName==""
					? "Choose Category"
					: props.categoryName}
				</DropdownToggle>
				<DropdownMenu>
					<DropdownItem
						onClick={(e)=>props.handleCategoryInput("SUV")}
						>SUV</DropdownItem>
						<DropdownItem
						onClick={(e)=>props.handleCategoryInput("MINIVAN")}
						>MINIVAN</DropdownItem>
						<DropdownItem
						onClick={(e)=>props.handleCategoryInput("VAN")}
						>VAN
					</DropdownItem>
				</DropdownMenu>
			</Dropdown>
		</FormGroup>
	)
}

export default CategoryInput;