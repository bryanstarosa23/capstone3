import React from 'react';
import {
	Modal,
	ModalHeader,
	ModalBody,
	Button
} from 'reactstrap';
import {FormInput} from '../../../globalcomponents'
import {CategoryInput} from '../components'
import Images from '../../Images/Images'

const VehicleForm = props => {
	return (
		<Modal
			isOpen={props.showForm}
			toggle={props.handleShowForm}
		>
			<ModalHeader
				toggle={props.handleShowForm}
				style={{"backgroundColor":"black", "color": "white"}}
			>
			Add Vehicle 
			</ModalHeader>
			<ModalBody>
				<FormInput
					label={"Plate No."}
					type={"text"}
					name={"plateNo"}
					placeholder={"Enter Plate No."}
					required={props.platenoRequired}
					onChange={props.handlePlateNoChange}
				/>
				<FormInput
					label={"Vehicle Name"}
					type={"text"}
					name={"name"}
					placeholder={"Enter Vehicle Name"}
					required={props.nameRequired}
					onChange={props.handleVehicleNameChange}
				/>
				<FormInput
					label={"Vehicle Description"}
					type={"text"}
					name={"description"}
					placeholder={"Enter Description"}
					required={props.descriptionRequired}
					onChange={props.handleVehicleDescriptionChange}
				/>
				<FormInput
					label={"Rent Price"}
					type={"number"}
					name={"rentPrice"}
					placeholder={"Rent Price"}
					required={props.rentPriceRequired}
					onChange={props.handleRentPriceChange}
				/>
				<CategoryInput 
					handleCategoryInput={props.handleCategoryInput}
					categoryName={props.categoryName}
					required={props.categoryRequired}
					onChange={props.handleVehicleCategoryChange}
				/>
				<Images
					selectImage={props.selectImage}
					infoMessage={props.infoMessage}
					categoryName={props.categoryName}
					name={props.name}
					description={props.description}
					plateno={props.plateno}
					vehicles={props.vehicles}
					handleRefresh={props.handleRefresh}
					rentPrice={props.rentPrice}
					invalidPlateNo={props.invalidPlateNo}
					setVehicles={props.setVehicles}

				/>
				{/* <Button
					color="warning"
					onClick={props.handleSaveVehicle}
					disabled={props.categoryName!="" && props.name!="" && props.description != "" && props.plateno != "" ? false : true}
				>Add Vehicle</Button> */}
			</ModalBody>
		</Modal>
	)
}

export default VehicleForm;