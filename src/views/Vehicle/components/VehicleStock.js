import React from 'react';
import _ from 'lodash';

const VehicleStock = props =>{
    const vehicles = props.vehicles;
    let suv = 0;
    let minivan= 0;
    let van = 0;
    // console.log(vehicles)
    vehicles.map(vehicle=>{
        if(vehicle.category == 'SUV'){
            suv += 1;
        }else if(vehicle.category == 'MINIVAN'){
            minivan +=1;
        }else{
            van +=1;
        }
    })
    return(
        <>
            <tr className="text-center">
                <td>SUV</td>
                <td>{suv}</td>
            </tr>
            <tr className="text-center">
                <td>MINIVAN</td>
                <td>{minivan}</td>
            </tr>
            <tr className="text-center">
                <td>VAN</td>
                <td>{van}</td>
            </tr>
        </>
    )
}

export default VehicleStock;