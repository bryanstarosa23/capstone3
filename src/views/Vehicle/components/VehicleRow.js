import React, {useState} from 'react';
import {
	Button,
	Dropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem,
} from 'reactstrap';
import {FormInput} from '../../../globalcomponents';
// import _ from 'lodash';



const VehicleRow = props => {
	
	const [dropdownOpen, setDropdownOpen]=useState(false);

	const toggle = () => setDropdownOpen(!dropdownOpen);

	const vehicle = props.vehicle;
	
	                                                
	return (
		<React.Fragment>
		
		<tr className="text-center">
			<td>{vehicle.plateNumber}</td>
			<td
				onClick={()=>props.handleNameEditInput(vehicle._id)}
			>{props.showName && props.editId === vehicle._id
				?
				 <FormInput 
					defaultValue={vehicle.name}
					type={"text"}
					onBlur={(e)=>props.handleEditName(e, vehicle._id)}
				/> 
				: vehicle.name
			}</td>
			<td
				onClick={()=>props.handleDescriptionEditInput(vehicle._id)}
			>{props.showDescription && props.editId === vehicle._id
				?
				 <FormInput 
					defaultValue={vehicle.description}
					type={"text"}
					onBlur={(e)=>props.handleEditDescription(e, vehicle._id)}
				/> 
				: vehicle.description
			}</td>
			<td onClick={()=>props.handleCategoryEditInput(vehicle._id)}
			>{props.showCategory && props.editId === vehicle._id
				?
				<Dropdown isOpen={dropdownOpen} toggle={toggle}>
					<DropdownToggle caret>
					{vehicle.category}
					</DropdownToggle>
					<DropdownMenu>
						<DropdownItem
						onClick={()=>props.handleEditCategory("SUV")}
						>SUV</DropdownItem>
						<DropdownItem
						onClick={()=>props.handleEditCategory("MINIVAN")}
						>MINIVAN</DropdownItem>
						<DropdownItem
						onClick={()=>props.handleEditCategory("VAN")}
						>VAN</DropdownItem>
					</DropdownMenu>
				</Dropdown>
				:
				vehicle.category}
			</td>
			<td
				onClick={()=>props.handleRentPriceEditInput(vehicle._id)}
			>{props.showRentPrice && props.editId === vehicle._id
				?
				 <FormInput 
					defaultValue={vehicle.rentPrice}
					type={"text"}
					onBlur={(e)=>props.handleEditRentPrice(e, vehicle._id)}
				/> 
				: vehicle.rentPrice
			}
			</td>
			<td>
			<Button
				color="danger"
				onClick={()=>props.handleDeleteVehicle(vehicle._id)}
			>
				Remove vehicle
			</Button>
			</td>
			

		</tr>
		</React.Fragment>
	)
}

export default VehicleRow;