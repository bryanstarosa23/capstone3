import React from 'react';
import {
	Modal,
	ModalHeader,
	ModalBody,
	Button,
	ListGroup,
	ListGroupItem
} from 'reactstrap'

const Logs = props => {
	return (
		<Modal
			isOpen={props.showLogs}
			toggle={props.handleShowLogs}
		>
			<ModalHeader
				toggle={props.handleShowLogs}
			>
			Logs
			</ModalHeader>
			<ModalBody>
				<h1
					className="text-center"
				>
					Logs for Request #{props.request.code}
				</h1>
				<h5>Asset Requested: {props.request.assetName}</h5>
				<h5>Requested By: {props.request.requestor}</h5>

				<h3 className="text-center py-3">Logs:</h3>
				<ListGroup>
					{props.request.logs.map(log=>
						<ListGroupItem
							key={log._id}
						>{log.message}<br/>{log.updateDate}</ListGroupItem>
						)}
				</ListGroup>

			</ModalBody>
		</Modal>
	)


}

export default Logs;
