import React, {useState} from 'react';
import {
	Button,
	Dropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem
} from 'reactstrap';
import {Logs} from '../components';
import {FormInput} from '../../../globalcomponents';

const RequestRow = (props) => {

	const [showLogs, setShowLogs] = useState(false);
	const [dropdownOpen, setDropdownOpen] = useState(false)

	const toggle = () => setDropdownOpen(!dropdownOpen)

	const request = props.request;

	const handleShowLogs = () => {
		setShowLogs(!showLogs);
	}

	return (
		<React.Fragment>
		<tr>
			<td>{request.code}</td>
			<td>{request.requestDate}</td>
			<td>{request.assetName}</td>
			<td
				onClick={()=>props.handleShowQuantity(request._id)}
			>{props.showQuantity && props.editId === request._id
				? <FormInput
					type={"number"} 
					defaultValue={request.quantity}
					onBlur={(e)=>props.handleEditQuantityInput(e, request)}
				/>
				:request.quantity}</td>
			<td>{request.requestor}</td>
			<td
				onClick={()=>props.handleShowStatus(request._id)}
			>{props.user.isAdmin && props.showStatus && props.editId == request._id
				? 
				<Dropdown
					isOpen={dropdownOpen}
					toggle={toggle}
				>
					<DropdownToggle caret>
					Update Status
					</DropdownToggle>
					<DropdownMenu>
						<DropdownItem 
						onClick={()=>props.handleStatusInput("Processing", request.logs)}>Processing</DropdownItem>
						<DropdownItem
						onClick={()=>props.handleStatusInput("Approved", request.logs)}
						>Approved</DropdownItem>
						<DropdownItem
						onClick={()=>props.handleStatusInput("Rejected", request.logs)}
						>Rejected</DropdownItem>
					</DropdownMenu>
				</Dropdown>
				: request.status
			}</td>
			<td>{request.approver}</td>
			<td>
				<Button
					color="danger"
					onClick={()=>props.handleDeleteRequest(request._id)}
				>Delete</Button>{" "}
				<Button
					color="info"
					onClick={handleShowLogs}
				>View Logs</Button>
			</td>
		</tr>
		<Logs 
			handleShowLogs={handleShowLogs}
			showLogs={showLogs}
			request={request}
		/>
		</React.Fragment>
		)
}

export default RequestRow;