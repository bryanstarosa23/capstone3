import React from 'react';
import {Elements, StripeProvider} from 'react-stripe-elements';
import CheckoutForm from './CheckoutForm';

const Payments = () => {
	return (
		<React.Fragment>
		<StripeProvider apiKey="pk_test_0ZLm93tap9XV5dUemOKaCALM00WWezGQ50">
			<div>
				<div>
					<Elements>
						<CheckoutForm 
							totalPayment={this.props.totalPayment}
							from = {this.props.from}
							to={this.props.to}
							vehicle={this.props.vehicle}
							userId={this.props.user.id}
							userName={this.props.user.name}
						/>
					</Elements>
				</div>
			</div>
		</StripeProvider>
		</React.Fragment>
	)
}

export default Payments;

