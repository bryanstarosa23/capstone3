import React, {useEffect} from 'react';
import {CardElement, injectStripe} from 'react-stripe-elements';
import moment from 'moment';
import axios from 'axios'

const CheckoutForm = (props) => {
	const submit = async (e) => {
        // console.log(e)
        
        let {token} = await props.stripe.createToken({
            name:"Name"})
        let tokenId = token.id
        let amount = 30000

        let data = new FormData;
        data.append('amount', 12345)

		axios.post('https://agile-cliffs-07835.herokuapp.com/charge',{
            amount: props.totalPayment,
            source: token.id
        }).then(res=>{
            console.log(res)
            const oneDay = 24 * 60 * 60 * 1000;
            const firstDate = new Date(props.days.from);
            const secondDate = new Date(props.days.to);
            const diffDays = Math.round(Math.abs((firstDate - secondDate) / oneDay));
            const totalPayment = parseInt(diffDays) * parseInt(props.vehicleRent);

            const user = props.userName;
            const id = props.userId;
            let code = moment(new Date()).format("x");

            
            axios({
                method: "POST",
                url: "https://agile-cliffs-07835.herokuapp.com/admin/addVehicleRequest",
                data: {
                  code: code,
                  requestor: user,
                  requestorId: id,
                  requestedVehicles: props.vehiclename,
                  requestedVehicleId: props.vehicleplatenumber,
                  dateNeeded: moment(props.days.from).format("MM/DD/YY"),
                  untilDate: moment(props.days.to).format("MM/DD/YY"),
                  totalPrice:totalPayment
                }
              }).then(res => {
                console.log(res.data);
              });
            })
        
	}

	return (
		<div className="checkout">
			<p>Amount: {props.totalPayment}</p>
        	<p>Would you like to complete the purchase?</p>
        	<CardElement />
        	<button onClick={submit}>Book</button>
      	</div>
	)
}

export default injectStripe(CheckoutForm);



