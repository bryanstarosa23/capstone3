import React from 'react';
import DayPicker from 'react-day-picker';

const Booking = () => {

	const handleDayClick = day => {
		console.log(day);
	}

	const disabledDays = {
		daysOfWeek: [0,6]
	}

	const past = {
		before: new Date()
	}

	return (
		<React.Fragment>
			<h1 className="text-center py-5">Book Now</h1>
			<div className="d-flex justify-content-center">
				<DayPicker 
					onDayClick={handleDayClick}
					showOutsideDays
					disabledDays={[past, disabledDays, new Date(2019, 10, 26)]}
				/>
			</div>
		</React.Fragment>
	)
}

export default Booking;