import React, {useState} from 'react';
import {FormInput} from '../../globalcomponents';
import {
	Button,
} from 'reactstrap';
import {Link} from 'react-router-dom';
import axios from 'axios';
import {Navbar, Footer} from '../Layout'


const Register = () => {


	const [name, setName] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const handleNameChange = (e) => {
		setName(e.target.value)
		console.log(e.target.value)
	}

	const handleEmailChange = (e) => {
		setEmail(e.target.value);
		console.log(e.target.value)
	}

	const handlePasswordChange = e => {
		setPassword(e.target.value);
		console.log(e.target.value);
	}

	const handleRegister = () => {
		axios.post("https://agile-cliffs-07835.herokuapp.com/register", {
			name: name,
			email: email,
			password: password
		}).then(res=>console.log(res.data))
	}

	return (

		<React.Fragment>
		<Navbar/>
			<div className="tunnel colorwhite">
				<div className="pt-5">
				<h1 className="text-center pt-5">Register</h1>
				<div className="col-lg-4 offset-lg-4 pb-5">
					<FormInput 
						label={"Name"}
						placeholder={"Enter your name"}
						type={"text"}
						name={"name"}
						onChange={handleNameChange}
					/>
					<FormInput 
						label={"Email"}
						placeholder={"Enter your email"}
						type={"email"}
						name={"email"}
						onChange={handleEmailChange}
					/>
					<FormInput 
						label={"Password"}
						placeholder={"Enter your password"}
						type={"password"}
						name={"password"}
						onChange={handlePasswordChange}
					/>
					<Button
						block
						color="info"
						onClick={handleRegister}
					>
						Register
					</Button>
					<Link className="nav-link" to="/login">Already have an account? Login here</Link>
				</div>
				</div>
			</div>
		<Footer/>
		</React.Fragment>

	)
}

export default Register;
