import React, {useState} from 'react';
import {FormInput} from '../../globalcomponents'
import {Button} from 'reactstrap'
import axios from 'axios'
import {Navbar, Footer} from '../Layout'

const Login = () => {

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const handleEmailChange = e => {
		setEmail(e.target.value);
	}

	const handlePasswordChange = e => {
		setPassword(e.target.value);
	}

	const handleLogin = () => {
		axios.post('https://agile-cliffs-07835.herokuapp.com/login',{
			email,
			password
		}).then(res=>{
			sessionStorage.token = res.data.token;
			sessionStorage.user = JSON.stringify(res.data.user)
			console.log(res.data.token)

			window.location.replace('#/userrequest')
		})
	}

	return (
		<React.Fragment>
			<Navbar/>
			<div className="d-flex containerHeight ">
				<div className="col-lg-6 loginImage">
				
				</div>
				<div className="col-lg-6 mt-5 pt-5">
					<div className="panel-group">
						<div className="col-lg-6 offset-lg-3 mt-5">
							<h4 className="text-center">Login</h4>
						</div>
						<div
						className="col-lg-6 offset-lg-3 mb-5"
						>
							<FormInput 
								label={"Email"}
								placeholder={"Enter your email"}
								type={"email"}
								onChange={handleEmailChange}
							/>
							<FormInput 
								label={"Password"}
								placeholder={"Enter your password"}
								type={"password"}
								onChange={handlePasswordChange}
							/>
							<Button
								block
								color="info"
								onClick={handleLogin}
							>
								Login
							</Button>
						</div>
					</div>
				</div>

			</div>
				
				
			<Footer/>
		</React.Fragment>
		)
}

export default Login;