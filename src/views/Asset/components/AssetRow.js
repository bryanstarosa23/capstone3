import React, {useState} from 'react';
import {
	Button,
	Dropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem,
} from 'reactstrap';
import {FormInput} from '../../../globalcomponents';


const AssetRow = props => {
	
	const [dropdownOpen, setDropdownOpen]=useState(false);

	const toggle = () => setDropdownOpen(!dropdownOpen);

	const asset = props.asset;

	return (
		<React.Fragment>
		
		<tr>
			<td>{asset.serialNumber}</td>
			<td>{asset.name}</td>
			<td>{asset.description}</td>
			<td onClick={()=>props.handleCategoryEditInput(asset._id)}
			>{props.showCategory && props.editId === asset._id
				?
				<Dropdown isOpen={dropdownOpen} toggle={toggle}>
					<DropdownToggle caret>
					{asset.category}
					</DropdownToggle>
					<DropdownMenu>
						<DropdownItem
						onClick={()=>props.handleEditCategory("Accessories")}
						>Accessories</DropdownItem>
						<DropdownItem
						onClick={()=>props.handleEditCategory("Hardware")}
						>Hardware</DropdownItem>
						<DropdownItem
						onClick={()=>props.handleEditCategory("Software")}
						>Software</DropdownItem>
					</DropdownMenu>
				</Dropdown>
				:
				asset.category}</td>
			<td
				onClick={()=>props.handleStockEditInput(asset._id)}

			>{props.showStock && props.editId === asset._id
				?
				 <FormInput 
					defaultValue={asset.stock}
					type={"number"}
					onBlur={(e)=>props.handleEditStock(e, asset._id)}
				/> 
				: asset.stock
			}</td>
			<td>
			<Button
				color="danger"
				onClick={()=>props.handleDeleteAsset(asset._id)}
			>
				Remove Asset
			</Button></td>
			

		</tr>
		</React.Fragment>
	)
}

export default AssetRow;