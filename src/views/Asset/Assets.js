import React, {useEffect, useState} from 'react';
import {AssetRow, AssetForm} from './components';
import {Loading} from '../../globalcomponents';
import {
	Button
} from 'reactstrap';
import moment from 'moment';
import axios from 'axios';
import {ToastContainer, toast} from 'react-toastify';
import {
	Navbar, Footer
} from "../Layout"

const invalidStock = () =>{
	toast.error("Invalid Stock")
}


const Assets = () => {
	const [assets, setAssets] = useState([]);
	const [isLoading, setIsLoading] = useState(true);
	const [showForm, setShowForm]= useState(false);
	const [nameRequired, setNameRequired] = useState(true);
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [descriptionRequired, setDescriptionRequired] = useState(true);
	const [category, setCategory] = useState("");
	const [categoryRequired, setCategoryRequired]=useState(true);
	const [stock, setStock]=useState(0);
	const [stockRequired, setStockRequired]=useState(true);
	const [showStock, setShowStock] = useState(true);
	const [showCategory, setShowCategory] = useState(true);
	const [editId, setEditId] = useState("");
	const [user, setUser] = useState({})


	const handleRefresh = () => {
		setShowForm(false);
		setName("");
		setCategory("");
		setDescription("");
		setStock(0);
		setNameRequired(true);
		setCategoryRequired(true);
		setDescriptionRequired(true);
		setStockRequired(true);
	}

	useEffect(()=>{
		// fetch('')

		if(sessionStorage.token){
			let user = JSON.parse(sessionStorage.user);
			if(user.isAdmin){
				fetch('http://localhost:4000/admin/showassets'
				).then(response=>response.json()
				).then(data=>{
					setAssets(data);
					setIsLoading(false);
				});

				setUser(user);

			}else{
				window.location.replace('/')
			}
		}else{
			window.location.replace('#/login')
		}
	},[])

	const handleShowForm = () =>{
		setShowForm(!showForm)
	}

	const handleAssetNameChange = (e) => {
		if(e.target.value==""){
			setNameRequired(true);
			setName("")
		}else{
			setNameRequired(false)
			setName(e.target.value);
		}
	}

	const handleAssetDescriptionChange = e => {
		if(e.target.value==""){
			setDescriptionRequired(true);
			setDescription("")
		}else{
			setDescriptionRequired(false)
			setDescription(e.target.value);
		}
	}

	const handleAssetCategoryChange = e => {
		if(e.target.value==""){
			setCategoryRequired(true);
			setCategory("")
		}else{
			setCategoryRequired(false)
			setCategory(e.target.value);
		}
	}

	const handleAssetStockChange = e => {
		if(e.target.value==""){
			setStockRequired(true);
			setStock(0)
		}else{
			setStockRequired(false)
			setStock(e.target.value);
		}
	}

	const handleSaveAsset = () => {
		let serialNumber = moment(new Date).format('x');

		axios({
			method: "POST",
			url: "http://localhost:4000/admin/addasset",
			data:{
				name:name,
				description:description,
				category:category,
				stock:parseInt(stock),
				serialNumber:serialNumber
			}
		}).then(res=>{
			// console.log(res.data);
			let newAssets = [...assets];
			newAssets.push(res.data);
			setAssets(newAssets);

		});

		handleRefresh();
	}

	const handleDeleteAsset = assetId => {
		axios({
			method: "DELETE",
			url: "http://localhost:4000/admin/deleteasset/" + assetId
		}).then(res=>{
			let newAssets = assets.filter(asset=>asset._id!=assetId);
			setAssets(newAssets)
		});
	}

	const handleEditStock = (e,assetId) => {
		let stock = e.target.value;
		// console.log(e.target.value)
		if(stock <= 0){
			setShowStock(true);
			invalidStock();
		}else{
			axios({
				method:'PATCH',
				url: 'http://localhost:4000/admin/updatestock/' + assetId,
				data: {
					stock: stock
				}
			}).then(res=>{
				let oldIndex;
				assets.forEach((asset,index)=>{
					if(asset._id === assetId){
						oldIndex=index;
					}
				});

				let newAssets = [...assets];
				newAssets.splice(oldIndex, 1, res.data);
				setAssets(newAssets);
				setShowStock(true);
				setEditId("");
			})
		}
	}

	const handleEditCategory = category => {
		axios({
			method: "PATCH",
			url: "http://localhost:4000/admin/updatecategory/" + editId,
			data: {
				category: category
			}
		}).then(res=>{
			let oldIndex;
				assets.forEach((asset,index)=>{
					if(asset._id === editId){
						oldIndex=index;
					}
				});

				let newAssets = [...assets];
				newAssets.splice(oldIndex, 1, res.data);
				setAssets(newAssets);
				setShowStock(true);
				setEditId("");
		})
	}

	const handleStockEditInput = editId => {
		setEditId(editId)
		setShowStock(true)
		setShowCategory(false)
	}

	const handleCategoryEditInput = editId => {
		setEditId(editId);
		setShowStock(false);
		setShowCategory(true);
	}

	return (
		<React.Fragment>
		<ToastContainer />
		{isLoading ?
			<Loading />
		:
		<React.Fragment>
		<Navbar />
			<div>
				<h1 
				className="text-center py-5">Assets</h1>
				<div className="d-flex justify-content-end pr-5 pb-4">
					<Button
					color="success"
					onClick={handleShowForm}
					>+ Add Asset</Button>
					<AssetForm 
						showForm={showForm}
						handleShowForm={handleShowForm}
						handleAssetNameChange={handleAssetNameChange}
						nameRequired={nameRequired}
						handleAssetDescriptionChange={handleAssetDescriptionChange}
						descriptionRequired={descriptionRequired}
						handleAssetCategoryChange={handleAssetCategoryChange}
						categoryRequired={categoryRequired}
						handleAssetStockChange={handleAssetStockChange}
						stockRequired={stockRequired}
						name={name}
						description={description}
						category={category}
						stock={stock}
						handleSaveAsset={handleSaveAsset}
					/>
				</div>
			</div>
			<table
				className="table table-striped border col-lg-10 offset-lg-1"
			>
				<thead>
					<tr>
						<th>Serial No.</th>
						<th>Name</th>
						<th>Description</th>
						<th>Category</th>
						<th>Stock</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{assets.map(asset=>
						<AssetRow 
							key={asset._id}
							asset={asset}
							handleDeleteAsset={handleDeleteAsset}
							setShowStock={setShowStock}
							showStock={showStock}
							handleEditStock={handleEditStock}
							handleStockEditInput={handleStockEditInput}
							editId={editId}
							handleCategoryEditInput={handleCategoryEditInput}
							showCategory={showCategory}
							handleEditCategory={handleEditCategory}
						/>
					)}
				</tbody>
			</table>
			<Footer />
		</React.Fragment>
		}
		</React.Fragment>
	)
}

export default Assets;