import React from 'react';
import ReactLoading from 'react-loading';



const Loading = () => {
	return (
		<div
			className="vh-100 vw-100 d-flex align-items-center justify-content-center bg-secondary">
			<ReactLoading type={"spokes"} color={"#221e1e"} height={'10%'} width={'10%'} />
		</div>
		
	)
}

export default Loading;