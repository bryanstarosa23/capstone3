import React from 'react';
import {
  HashRouter,
  Route,
  Switch
} from 'react-router-dom';
import ReactLoading from 'react-loading';



const loading = () => {
  return (
  <div className="bg-warning d-flex justify-content-center align-items-center">
    <h1>Loading...</h1>
  </div>
  )
}

const Example = () => (
  <div className="mt-5 vh-100 vw-100 d-flex justify-content-center align-items-center">
  <ReactLoading type={"spinningBubbles"} color={"#221e1e"}  />
  </div>
);

// 
const Register = React.lazy(()=>import('./views/Pages/Register'));
const Login = React.lazy(()=>import('./views/Pages/Login'));
const Assets = React.lazy(()=>import('./views/Asset/Assets'));
const Requests = React.lazy(()=>import('./views/Request/Requests'));
const Home = React.lazy(()=>import('./views/Layout/Home'))
const Images = React.lazy(()=>import('./views/Images/Images'))
const Booking = React.lazy(()=>import('./views/Booking/Booking'))
const Payments = React.lazy(()=>import('./views/Payments/Payments'))
const Vehicles = React.lazy(()=>import('./views/Vehicle/Vehicle'))
const Userrequest = React.lazy(()=>import('./views/Requestedvehicle/Userrequest'))
const Requestedvehicles = React.lazy(()=>import('./views/Requestedvehicle/Requestedvehicle'))

const App = () => {
  return (
    <HashRouter>
      <React.Suspense fallback={Example()}>
        <Switch>
          <Route 
            path="/"
            exact
            name="Home"
            render={props=><Home />}
          />
          <Route 
            path="/register"
            exact
            name="Register"
            render={props => <Register {...props} />}
          />
          <Route 
            path="/login"
            exact
            name="Login"
            render={props => <Login {...props}/>}
          />
          <Route 
            path="/assets"
            exact
            name="Assets"
            render={props=> <Assets 
                {...props}
              />}
          />
          <Route 
            path="/requests"
            exact
            name="Requests"
            render={props=> <Requests />}
          />
          <Route 
            path="/userrequest"
            exact
            name="UserRequest"
            render={props=><Userrequest />}
          />
          <Route 
            path="/requestedvehicles"
            exact
            name="Requested Vehicle"
            render={props=> <Requestedvehicles />}
          />
          <Route 
            path="/vehicles"
            exact
            name="Vehicles"
            render={props=> <Vehicles />}
          />
          
          <Route 
            path="/upload"
            exact
            name="Upload Images"
            render={props=><Images {...props}/>}
          />
          <Route 
            path="/book"
            exact
            name="Book Now"
            render={props=><Booking {...props}/>}
          />
          <Route 
            path="/stripe"
            exact
            name="Stripe"
            render={props=><Payments {...props}/>}
          />
        </Switch>
      </React.Suspense>
    </HashRouter>
  )
}

export default App;

